call plug#begin()

    Plug 'gmarik/Vundle.vim'                           " Vundle
    Plug 'itchyny/lightline.vim'                       " Lightline statusbar
    Plug 'vifm/vifm.vim'                               " Vifm

call plug#end()

let g:lightline = {
          \ 'colorscheme': 'darcula',
      \ }

set laststatus=2

map <Leader>vv :Vifm<CR>
map <Leader>vs :VsplitVifm<CR>
map <Leader>sp :SplitVifm<CR>
map <Leader>dv :DiffVifm<CR>
map <Leader>tv :TabVifm<CR>

set nocompatible
set expandtab
set number
set tabstop=4
set softtabstop=4
set shiftwidth=4
set cursorline
set wildmenu
set showmatch
set nowrap

syntax on
filetype on
