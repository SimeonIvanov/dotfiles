# Dotfiles

Personal dotfiles I use on my Arch Linux system. Anyone can clone the repository to use these files.

![Screenshot](screenshot.png)
![Another screenshot](screenshot_1.png)

## Main config files

 - [Qtile config](https://gitlab.com/SimeonIvanov/dotfiles/-/tree/main/.config/qtile)
 - [Alacritty config](https://gitlab.com/SimeonIvanov/dotfiles/-/tree/main/.config/alacritty)
 - [Neofetch config](https://gitlab.com/SimeonIvanov/dotfiles/-/tree/main/.config/neofetch)

## Other repos

 - [dmenu](https://gitlab.com/SimeonIvanov/patched-dmenu)
