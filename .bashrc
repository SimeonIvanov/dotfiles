#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Basic commands
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../..'
alias ls='exa -al --color=always --group-directories-first'
alias mkdir='mkdir -pv'

# Interactive mode
alias mv='mv -i'
alias cp='cp -i'
alias rm='rm -i'
alias ln='ln -i'

#Open directories
alias cdconf='cd ~/.config/'
alias cdqtile='cd ~/.config/qtile/'
alias cdawesome='cd ~/.config/awesome/'

#Edit configuration files
alias alacrittyconf='vim ~/.config/alacritty/alacritty.yml'
alias picomconf='vim ~/.config/picom/picom.conf'
alias neofetchconf='vim ~/.config/neofetch/config.conf'
alias qtileconf='vim ~/.config/qtile/config.py'
alias awesomeconf='vim ~/.config/awesome/rc.lua'
alias dunstconf='vim ~/.config/dunst/dunstrc'
alias bashrc='vim ~/.bashrc'
alias vimrc='vim ~/.vimrc'

#Miscellaneous
alias notification='dunstify PIPI "`tail ~/Downloads/PIPI.txt`"' 
# this is an alias to test notifications, the file contains this copypasta: https://copypastatext.com/tag/tigran-petrosyan/

PS1='\W / \$ '
PS2='$>>'

neofetch
