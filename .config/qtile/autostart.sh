#!/usr/bin/env bash

lxsession &
picom & #--vsync --experimental-backends &
#conky -c $HOME/.config/conky/conky.conf &
dunst &
#nm-applet &
connman-gtk &
nitrogen --restore &
ffplay -nodisp -autoexit -af "volume=2" ~/.config/qtile/sounds/service-login.oga &

