# 0: background
# 1: foreground
# 2: background lighter
# 3: red
# 4: green
# 5: yellow
# 6: blue
# 7: magenta
# 8: cyan
# 9: white
# 10: grey
# 11: orange
# 12: focused 
# 13: unfocused
nord = [
        "#2e3440",
        "#d8dee9",
        "#3b4252",
        "#bf616a",
        "#a3be8c",
        "#ebcb8b",
        "#81a1c1",
        "#b48ead",
        "#88c0d0",
        "#e5e9f0",
        "#4c566a",
        "#d08770",
        "#8fbcbb",
        "#242831",
]
dracula = [
        "#282a36",
        "#f8f8f2",
        "#44475a",
        "#ff5555",
        "#50fa7b",
        "#f1fa8c",
        "#bd93f9",
        "#ff79c6",
        "#8be9fd",
        "#f8f8f2",
        "#44475a",
        "#ffb86c",
        "#6272a4",
        "#282a36",
]
# Some colors are switched:
# yellow is blue
# green is yellow
# cyan is green
# blue is cyan
monokaipro = [
        "#2d2a2e",
        "#fcfcfa",
        "#363537",
        "#ff6188",
        "#ffd866",
        "#fc9867",
        "#78dce8",
        "#ab9df2",
        "#a9dc76",
        "#fcfcfa",
        "#908e8f",
        "#fc9867",
        "#fc9867",
        "#2d2a2e",
]
gruvboxdark = [
        "#282828",
        "#ebdbb2",
        "#504945",
        "#cc241d",
        "#98971a",
        "#d79921",
        "#458588",
        "#b16286",
        "#689d6a",
        "#fbf1c7",
        "#a89984",
        "#d65d0e",
        "#8ec07c",
        "#32302f",
]
catppuccin = [
        "#1e1d2f",
        "#d9e0ee",
        "#302d41",
        "#f28fad",
        "#abe9b3",
        "#fae3b0",
        "#96cdfb",
        "#f5c2e7",
        "#89dceb",
        "#d9e0ee",
        "#6e6c7e",
        "#f8bd96",
        "#c9cbff",
        "#575268",
]
nordlight = [
        "#d8dee9",
        "#2e3440",
        "#4c566a",
        "#bf616a",
        "#a3be8c",
        "#ebcb8b",
        "#81a1c1",
        "#b48ead",
        "#88c0d0",
        "#e5e9f0",
        "#434c5e",
        "#d08770",
        "#8fbcbb",
        "#242831",
]
