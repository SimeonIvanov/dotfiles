##############
#QTILE CONFIG#
##############
import os
import subprocess

from typing import List  # noqa: F401

from libqtile import qtile, bar, layout, widget, hook
from libqtile.config import Click, Drag, Group, Key, Match, Screen
from libqtile.lazy import lazy
#from libqtile.utils import guess_terminal

from colors import *
from default import *
from bar_one import screens

keys = [
    # Switch between windows
    Key([mod], "h", lazy.layout.left(), desc = "Move focus to left"),
    Key([mod], "l", lazy.layout.right(), desc = "Move focus to right"),
    Key([mod], "j", lazy.layout.down(), desc = "Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc = "Move focus up"),
    Key([mod], "m", lazy.layout.next(),desc = "Move window focus to other window"),

    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([mod, "shift"], "h", lazy.layout.shuffle_left(), desc = "Swap with the window to the left"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(), desc = "Swap with the window to the right"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(), desc = "Swap with the  window below"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc = "Swap with the  window above"),
    Key([mod, "shift"], "o", lazy.layout.flip(), desc = "Flip the master and the stack windows"),

    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key([mod, "control"], "h", lazy.layout.shrink_main(), desc = "Shrink master window"),
    Key([mod, "control"], "j", lazy.layout.shrink(), desc = "Shrink window"),
    Key([mod, "control"], "k", lazy.layout.grow(), desc = "Grow window"),
    Key([mod, "control"], "l", lazy.layout.grow_main(), desc = "Grow master window"),
    Key([mod], "n", lazy.layout.normalize(), desc = "Reset all window sizes"),

    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout(), desc = "Next layout"),
    Key([mod, "shift"], "Tab", lazy.prev_layout(), desc = "Previous layout"),

    Key([mod, "shift"], "c", lazy.window.kill(), desc = "Kill focused window"),
    Key([mod, "control"], "r", lazy.restart(), desc = "Restart Qtile"),
    Key([mod, "control"], "q", lazy.shutdown(), desc = "Shutdown Qtile"),

    # Spawn programs
    Key([mod], "Return", lazy.spawn(my_term), desc = "Launch terminal"),
    Key([mod], "b", lazy.spawn(my_browser), desc = "Launch browser"),
    Key([mod], "p", lazy.spawn(run_launcher), desc = "Run launcher"),
    Key([mod], "f", lazy.spawn(my_fm), desc = "Launch file manager"),
]

groups = [Group(i) for i in "123456789"]

for i in groups:
    keys.extend([
        # mod1 + letter of group = switch to group
        # mod1 + shift + letter of group = switch to & move focused window to group
        # mod1 + shift + letter of group = move focused window to group
        Key([mod], i.name, lazy.group[i.name].toscreen(),
            desc = "Switch to group {}".format(i.name)),
        Key([mod, "shift"], i.name, lazy.window.togroup(i.name, switch_group = True),
            desc = "Switch to & move focused window to group {}".format(i.name)),
        Key([mod, "control"], i.name, lazy.window.togroup(i.name),
             desc="move focused window to group {}".format(i.name)),
    ])

layouts = [
    layout.MonadTall(** layout_theme, name = ""),
    layout.Max(name = ""),
    layout.Floating(** layout_theme, name = ""),

    # layout.Bsp(** layout_theme, name = ""),
    # layout.Columns(** layout_theme),
    # layout.Stack(num_stacks = 3,** layout_theme),
    # layout.Matrix(** layout_theme),
    # layout.MonadWide(** layout_theme),
    # layout.RatioTile(** layout_theme),
    # layout.Tile(** layout_theme),
    # layout.TreeTab(** layout_theme),
    # layout.VerticalTile(** layout_theme),
    # layout.Zoomy(** layout_theme),
]

#screens = [
#    Screen(
#        top = bar.Bar(
#            [
#                widget.GroupBox(
#                    ** group_box_settings,
#                    fontsize = 12,
#                    font = text_font,
#                ),
#            ],
#            28,
#            background = colors[0],
#            margin = [0, 0, 0, 0],
#        )
#    ),
#]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start = lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start = lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(border_width = 2, border_focus = colors[12], border_normal = colors[13], float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    *layout.Floating.default_float_rules,
    Match(wm_class = 'confirmreset'),  # gitk
    Match(wm_class = 'makebranch'),  # gitk
    Match(wm_class = 'maketag'),  # gitk
    Match(wm_class = 'ssh-askpass'),  # ssh-askpass
    Match(title = 'branchdialog'),  # gitk
    Match(title = 'pinentry'),  # GPG key password entry
])
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

@hook.subscribe.startup
def autostart():
        home = os.path.expanduser('~/.config/qtile/autostart.sh')
        subprocess.call([home])

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "qtile"
