import os
import subprocess
try:
    import psutil
except ModuleNotFoundError:
    pass

from libqtile import qtile, bar, widget
from libqtile.config import Screen
from libqtile.lazy import lazy

from default import *

screens = [
    Screen(
        top = bar.Bar(
            [
                #
                #LEFT SIDE
                #
                widget.Image(
                    #filename = "~/.config/qtile/icons/Arch-linux-logo.png",
                    filename = "~/.config/qtile/icons/Artix-linux-logo.png",
                    scale = True,
                    margin = 3,
                    mouse_callbacks = {
                        "Button1": lazy.spawn(run_launcher),
                        "Button3": lazy.spawn(run_launcher),
                    },
                ),
                widget.GroupBox(
                    ** group_box_settings,
                    fontsize = 12,
                    font = text_font,
                ),
                widget.Sep(
                    linewidth = 0,
                    padding = 10,
                    size_percent = 40,
                ),
                widget.CurrentLayout(
                    ** icon_settings,
                    foreground = colors[1],
                ),
                widget.Sep(
                    linewidth = 0,
                    padding = 10,
                    size_percent = 40,
                ),
                widget.WidgetBox(
                    widgets = [
                        widget.Sep(
                            linewidth = 2,
                            padding = 10,
                            background = colors[2],
                        ),
                        widget.Systray(
                            icon_size = icon_size,
                            padding = 0,
                            background = colors[2],
                        ),
                        widget.Sep(
                            linewidth = 2,
                            padding = 10,
                            background = colors[2],
                        ),
                    ],
                    ** icon_settings,
                    close_button_location = "right",
                    text_open = "",
                    text_closed = "",
                    foreground = colors[1],
                ),
                widget.Sep(
                    linewidth = 0,
                    padding = 10,
                    size_percent = 40,
                ),
                widget.WindowName(
                    foreground = colors[1],
                    fontsize = text_size,
                    font = text_font,
                ),
                widget.Spacer(),
                #
                #THE MIDDLE
                #
                widget.TextBox(
                    ** icon_settings,
                    text = "",
                    foreground = colors[1],
                ),
                widget.Clock(
                    format = "%b %d  %H:%M ",
                    foreground = colors[1],
                    fontsize = text_size,
                    font = text_font,
                ),
                widget.TextBox(
                    ** icon_settings,
                    text = "",
                    foreground = colors[1],
                ),
                #
                #RIGHT SIDE
                #
                widget.Spacer(
                ),
                widget.TextBox(
                    ** slash_settings,
                    foreground = colors[3],
                ),
                widget.TextBox(
                    ** icon_settings,
                    text = "",
                    foreground = colors[3],
                    mouse_callbacks = {"Button3": lazy.spawn("pavucontrol")},
                ),
                widget.PulseVolume(
                    foreground = colors[3],
                    limit_max_volume = "True",
                    update_interval = 0.1,
                    fontsize = text_size,
                    font = text_font,
                    mouse_callbacks = {"Button3": lazy.spawn("pavucontrol")},
                ),
                widget.TextBox(
                    ** slash_settings,
                    foreground = colors[5],
                ),
                widget.TextBox(
                    ** icon_settings,
                    text = "",
                    foreground = colors[5],
                    mouse_callbacks = {
                        "Button1": lazy.spawn(my_term + " -e htop"),
                        "Button3": lazy.spawn(my_term + " -e htop"),
                    },
                ),
                widget.CPU(
                    foreground = colors[5],
                    update_interval = 1,
                    format = "{load_percent: .0f} %",
                    fontsize = text_size,
                    font = text_font,
                    mouse_callbacks = {
                        "Button1": lazy.spawn(my_term + " -e htop"),
                        "Button3": lazy.spawn(my_term + " -e htop"),
                    },
                ),
                widget.TextBox(
                    ** slash_settings,
                    foreground = colors[4],
                ),
                widget.TextBox(
                    ** icon_settings,
                    text = "",
                    foreground = colors[4],
                    mouse_callbacks = {
                        "Button1": lazy.spawn(my_term + " -e htop"),
                        "Button3": lazy.spawn(my_term + " -e htop"),
                    },
                ),
                widget.Memory(
                    foreground = colors[4],
                    format = "{MemPercent: .0f} %",
                    fontsize = text_size,
                    font = text_font,
                    mouse_callbacks = {
                        "Button1": lazy.spawn(my_term + " -e htop"),
                        "Button3": lazy.spawn(my_term + " -e htop"),
                    },
                ),
                widget.TextBox(
                    ** slash_settings,
                    foreground = colors[8],
                ),
                widget.TextBox(
                    ** icon_settings,
                    text = "",
                    foreground = colors[8],
                ),
                widget.OpenWeather(
                    location = "gabrovo,BG",
                    format = "{main_temp} °{units_temperature}, {humidity}%, {weather_details}",
                    foreground = colors[8],
                    fontsize = text_size,
                    font = text_font,
                ),
                widget.TextBox(
                    ** slash_settings,
                    foreground = colors[6],
                ),
                widget.TextBox(
                    ** icon_settings,
                    text = "",
                    foreground = colors[6],
                ),
                widget.Battery(
                    format = "{char} {percent:2.0%}",
                    foreground = colors[6],
                    fontsize = text_size,
                    font = text_font,
                ),
                widget.TextBox(
                    ** slash_settings,
                    foreground = colors[7],
                ),
                widget.TextBox(
                    ** icon_settings,
                    text = "",
                    foreground = colors[7],
                    mouse_callbacks = {
                        "Button1": lazy.spawn(my_term + " -e sudo pacman -Syu"),
                        "Button3": lazy.spawn(my_term + " -e sudo pacman -Syu"),
                    },
                ),
                widget.CheckUpdates(
                    distro = "Arch",
                    update_interval = 1800,
                    no_update_string = "No updates",
                    colour_have_updates = colors[7],
                    colour_no_updates = colors[7],
                    fontsize = text_size,
                    font = text_font,
                    mouse_callbacks = {
                        "Button1": lazy.spawn(my_term + " -e sudo pacman -Syu"),
                        "Button3": lazy.spawn(my_term + " -e sudo pacman -Syu"),
                    },
                ),
                widget.Sep(
                    linewidth = 0,
                    padding = 10,
                    size_percent = 40,
                ),
                widget.QuickExit(
                    **icon_settings,
                    default_text = "[]",
                    foreground = colors[1],
                    countdown_start = 1,
                ),
            ],
            28,
            background = colors[0],
            margin = [0, 0, 0, 0],
        ),
    ),
]
