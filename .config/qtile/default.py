from colors import *

text_size = 14
icon_size = 18
text_font = "Ubuntu Light"
icon_font = "Font Awesome 6 Free Solid"

colors = dracula

mod = "mod4"    #super key
my_term = "alacritty"
my_fm = "pcmanfm"
my_browser = "firefox"
#run launcher
run_launcher = "dmenu_run "                 # name of launcher
run_launcher += "-fn \"" + text_font + "\" "# font
run_launcher += "-nb \"" + colors[0] + "\" "# normal background of dmenu
run_launcher += "-nf \"" + colors[1] + "\" "# normal foreground of dmenu
run_launcher += "-sb \"" + colors[4] + "\" "# selection background of dmenu
run_launcher += "-sf \"" + colors[0] + "\" "# selection foreground of dmenu
run_launcher += "-l 16 -p 'Run: '"          # run prompt of dmenu

layout_theme = {
    "border_width": 2,
    "margin": 10,
    "border_focus": colors[12],
    "border_normal": colors[13],
    "single_border_width": 2
}

widget_defaults = {
    "font": 'sans',
    "fontsize": text_size,
    "padding": 3,
}
extension_defaults = widget_defaults.copy()

group_box_settings = {
    "active": colors[1],
    "inactive": colors[10],
    "highlight_color": colors[2],
    "block_highlight_text_color": colors[1],
    "this_current_screen_border": colors[11],
    "this_screen_border": colors[7],
    "other_current_screen_border": colors[12],
    "other_screen_border": colors[0],
    "foreground": colors[1],
    "background": colors[0],
    "urgent_border": colors[3],
    "highlight_method": "border",
    "padding": 5,
    "borderwidth": 3,
    "disable_drag": True,
    "rounded": True,
}

slash_settings = {
    "text": '/',
    "padding": 0,
    "fontsize": 48,
}

icon_settings = {
    "font": icon_font,
    "fontsize": icon_size,
}

